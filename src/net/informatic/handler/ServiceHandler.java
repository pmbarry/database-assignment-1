package net.informatic.handler;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.informatic.bean.ServiceBean;
import net.informatic.dao.ServiceDao;


/**
 * Servlet implementation class FleetHandler
 */
public class ServiceHandler extends HttpServlet { 
		private static final long serialVersionUID = 1L;
	
	    private static String Insert = "/service.jsp";
	    private static String Edit = "/editService.jsp";
	    private static String ServiceRecord = "/listService.jsp";
	    private ServiceDao dao;
	    
	    public ServiceHandler() {
	        super();
	        dao = new ServiceDao();
	    }

	    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    	
	    	Enumeration<String> params = request.getParameterNames(); 
	    	while(params.hasMoreElements()){
	    	 String paramName = params.nextElement();
	    	 System.out.println("Parameter Name - "+paramName+", Value - "+request.getParameter(paramName));
	    	}
	    	
	    	String redirect="";
	        String serviceDate = request.getParameter("service_date");        
	        String action = request.getParameter("action");
	        
	        if(!((serviceDate)== null) && action.equalsIgnoreCase("insert"))
	        {
	        	
	        	ServiceBean serviceBean = new ServiceBean();
	        	
	        	serviceBean.setServiceDate(request.getParameter("service_date"));
	        	serviceBean.setServiceVehicleId(Integer.parseInt(request.getParameter("service_vehicle_id")));
	        	serviceBean.setServiceOdometer(Integer.parseInt(request.getParameter("service_odo")));
	        	
	        	
	          
	        	dao.addDepot(serviceBean);
	        	redirect = ServiceRecord;
	            request.setAttribute("services", dao.getAllServices());    
	           	System.out.println("Record Added Successfully");
	        }
	        else if (action.equalsIgnoreCase("delete")){
	            String Id = request.getParameter("service_id");
	            System.out.println(Id);
	            int id = Integer.parseInt(Id);
	            dao.removeService(id);
	            redirect = ServiceRecord;
	            request.setAttribute("depots", dao.getAllServices());
	            System.out.println("Record Deleted Successfully");
	            
	        }else if (action.equalsIgnoreCase("editform")){        	
	        	redirect = Edit; 
	        	
	        } else if (action.equalsIgnoreCase("edit")){
	        	String Id = request.getParameter("service_id");
	            int id = Integer.parseInt(Id);            
	            ServiceBean serviceBean = new ServiceBean();
	        	serviceBean.setServiceId(id);
	        	serviceBean.setServiceDate(request.getParameter("service_date"));
	        	serviceBean.setServiceVehicleId(Integer.parseInt(request.getParameter("service_vehicle_id")));
	        	serviceBean.setServiceOdometer(Integer.parseInt(request.getParameter("service_odometer")));
	            dao.editService(serviceBean);
	            request.setAttribute("services", serviceBean);
	            redirect = ServiceRecord;
	            System.out.println("Record updated Successfully");
	            
	         } else if (action.equalsIgnoreCase("listService")){
	            redirect = ServiceRecord;
	            request.setAttribute("services", dao.getAllServices());
	         } else {
	            redirect = Insert;
	        }

	        RequestDispatcher rd = request.getRequestDispatcher(redirect);
	        rd.forward(request, response);
	    }

	    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	        doGet(request, response);
	    }

}
