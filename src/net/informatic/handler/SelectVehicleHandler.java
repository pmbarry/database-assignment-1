package net.informatic.handler;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.informatic.dao.SelectVehicleDao;
import net.informatic.bean.SelectVehicleBean;

public class SelectVehicleHandler extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	private void listCategory(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		SelectVehicleDao dao = new SelectVehicleDao();

		try {

			List<SelectVehicleBean> listVehicle = dao.list();
			System.out.println(listVehicle.iterator());
			request.setAttribute("listVehicle", listVehicle);
			

			RequestDispatcher dispatcher = request.getRequestDispatcher("/service.jsp");
			dispatcher.forward(request, response);

		} catch (SQLException e) {
			e.printStackTrace();
			throw new ServletException(e);
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int vehicleId = Integer.parseInt(request.getParameter("vehicle"));

		request.setAttribute("selectedVehicleId", vehicleId);

		listCategory(request, response);
	}
}
