package net.informatic.handler;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.informatic.dao.SelectManagerDao;
import net.informatic.bean.SelectManagerBean;


/**
 * Servlet implementation class SelectManagerHandler
 */
public class SelectManagerHandler extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectManagerHandler() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}
	private void listManager(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		SelectManagerDao dao = new SelectManagerDao();

		try {

			List<SelectManagerBean> listManager = dao.list();
			System.out.println(listManager.iterator());
			request.setAttribute("listManager", listManager);
			

			RequestDispatcher dispatcher = request.getRequestDispatcher("/driver.jsp");
			dispatcher.forward(request, response);

		} catch (SQLException e) {
			e.printStackTrace();
			throw new ServletException(e);
		}
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int managerId = Integer.parseInt(request.getParameter("manager"));

		request.setAttribute("selectedManagerId", managerId);

		listManager(request, response);
	}
	}


