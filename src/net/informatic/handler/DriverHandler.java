package net.informatic.handler;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.informatic.dao.DriverDao;
import net.informatic.bean.DriverBean;

public class DriverHandler extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static String Insert = "/driver.jsp";
	private static String Edit = "/editDriver.jsp";
	private static String DriverRecord = "/listDriver.jsp";
	private DriverDao dao;

	public DriverHandler() {
		super();
		dao = new DriverDao();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Enumeration<String> params = request.getParameterNames();
		while (params.hasMoreElements()) {
			String paramName = params.nextElement();
			System.out.println("Parameter Name - " + paramName + ", Value - " + request.getParameter(paramName));
		}

		String redirect = "";
		String driverFirstName = request.getParameter("driver_firstname");
		String action = request.getParameter("action");

		if (!((driverFirstName) == null) && action.equalsIgnoreCase("insert")) {
			
			
			DriverBean driver = new DriverBean();
			
			driver.setDriverFirst(request.getParameter("driver_firstname"));
			driver.setDriverSurname(request.getParameter("driver_surname"));
			driver.setDriverPoints(Integer.parseInt(request.getParameter("driver_points")));
			driver.setDriverAllowance(Integer.parseInt(request.getParameter("driver_allowance")));
			driver.setDriverManagerId(Integer.parseInt(request.getParameter("driver_manager_id")));
			driver.setDriverUserName(request.getParameter("driver_user"));
			driver.setDriverPassword(request.getParameter("driver_password"));

			dao.addDriver(driver);
			redirect = DriverRecord;
			request.setAttribute("drivers", dao.getAllDrivers());
			System.out.println("Record Added Successfully");
		}

		else if (action.equalsIgnoreCase("delete")) {
			String id = request.getParameter("driverId");
			int did = Integer.parseInt(id);
			System.out.println(did);
			dao.removeDriver(did);
			redirect = DriverRecord;
			request.setAttribute("drivers", dao.getAllDrivers());
			System.out.println("Record Deleted Successfully");

		} else if (action.equalsIgnoreCase("editform")) {
			redirect = Edit;

		} else if (action.equalsIgnoreCase("edit")) {
			String id = request.getParameter("driver_id");
			int did = Integer.parseInt(id);
			DriverBean driver = new DriverBean();
			driver.setDriverId(did);
			driver.setDriverFirst(request.getParameter("driver_firstname"));
			driver.setDriverSurname(request.getParameter("driver_surname"));
			driver.setDriverPoints(Integer.parseInt(request.getParameter("driver_points")));
			driver.setDriverAllowance(Integer.parseInt(request.getParameter("driver_allowance")));
			driver.setDriverManagerId(Integer.parseInt(request.getParameter("driver_manager_id")));
			driver.setDriverUserName(request.getParameter("driver_user"));
			driver.setDriverPassword(request.getParameter("driver_password"));
			dao.editDriver(driver);
			request.setAttribute("driver", driver);
			redirect = DriverRecord;
			System.out.println("Record updated Successfully");

		} else if (action.equalsIgnoreCase("listDriver")) {
			redirect = DriverRecord;
			request.setAttribute("drivers", dao.getAllDrivers());
		} else {
			redirect = Insert;
		}

		RequestDispatcher rd = request.getRequestDispatcher(redirect);
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}
