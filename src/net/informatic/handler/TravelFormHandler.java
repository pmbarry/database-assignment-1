package net.informatic.handler;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.informatic.dao.TravelFormDao;
import net.informatic.bean.TravelFormBean;

import net.informatic.bean.TravelLogBean;

public class TravelFormHandler extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static String Insert = "/travelForm.jsp";
	/*
	 * private static String Edit = "/editTravelForm.jsp"; private static String
	 * DriverRecord = "/listTravelForm.jsp";
	 */
	private TravelFormDao dao;

	public TravelFormHandler() {
		super();
		dao = new TravelFormDao();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Enumeration<String> params = request.getParameterNames();
		while (params.hasMoreElements()) {
			String paramName = params.nextElement();
			System.out.println("Parameter Name - " + paramName + ", Value - " + request.getParameter(paramName));
		}

		String redirect = "";
		String action = request.getParameter("action");

		String logCount = request.getParameter("log-count");

		if (!((logCount) == null) && action.equalsIgnoreCase("insert")) {

			TravelFormBean travelForm = new TravelFormBean();
			travelForm.setTravelFormDriver(Integer.parseInt(request.getParameter("Driver")));
			travelForm.setTravelFormDate(request.getParameter("Date"));
			travelForm.setTravelFormWeek(Integer.parseInt(request.getParameter("Week")));
			int m = Integer.parseInt(request.getParameter("Miles"));
			System.out.println(m);
			travelForm.setTravelFormMiles(Integer.parseInt(request.getParameter("Miles")));

			dao.addForm(travelForm);
			/*
			 * redirect = FormRecord; request.setAttribute("drivers", dao.getAllDrivers());
			 */
			System.out.println("Form Record Added Successfully");

			/* Add Travel Logs */

			int cid = Integer.parseInt(logCount);
			int fid = travelForm.getTravelFormId();
			for (int i = 0; i <= cid; i++) {
				TravelLogBean travelLogBean = new TravelLogBean();

				System.out.println("Depot name parameter" + (Integer.parseInt(request.getParameter("Depot" + i))));
				travelLogBean.setTravelLogDepotId(Integer.parseInt(request.getParameter("Depot" + i)));
				travelLogBean.setTravelLogDate(request.getParameter("Date" + i));
				travelLogBean.setTravelLogPurpose(request.getParameter("Purpose" + i));
				travelLogBean.setTravelLogMiles(Integer.parseInt(request.getParameter("Kilometers" + i)));
				travelLogBean.setTravelLogFormId(fid);
				travelLogBean.setTravelLogVehicleId(Integer.parseInt(request.getParameter("Vehicle" + i)));

				dao.addTravelLog(travelLogBean);
				System.out.println("Log Record Added Successfully");

			}
		}

		/*
		 * else if (action.equalsIgnoreCase("delete")){ String id =
		 * request.getParameter("driverId"); int did = Integer.parseInt(id);
		 * System.out.println(did); dao.removeDriver(did); redirect = DriverRecord;
		 * request.setAttribute("drivers", dao.getAllDrivers());
		 * System.out.println("Record Deleted Successfully");
		 * 
		 * }else if (action.equalsIgnoreCase("editform")){ redirect = Edit;
		 * 
		 * } else if (action.equalsIgnoreCase("edit")){ String id =
		 * request.getParameter("driver_id"); int did = Integer.parseInt(id); DriverBean
		 * driver = new DriverBean(); driver.setDriverId(did);
		 * driver.setDriverFirst(request.getParameter("driver_firstname"));
		 * driver.setDriverSurname(request.getParameter("driver_surname"));
		 * driver.setDriverAddressId(Integer.parseInt(request.getParameter(
		 * "driver_address_id")));
		 * driver.setDriverPoints(Integer.parseInt(request.getParameter("driver_points")
		 * )); driver.setDriverAllowance(Integer.parseInt(request.getParameter(
		 * "driver_allowance")));
		 * driver.setDriverManagerId(Integer.parseInt(request.getParameter(
		 * "driver_manager_id")));
		 * driver.setDriverUserName(request.getParameter("driver_user"));
		 * driver.setDriverPassword(request.getParameter("driver_password"));
		 * dao.editDriver(driver); request.setAttribute("driver", driver); redirect =
		 * DriverRecord; System.out.println("Record updated Successfully");
		 * 
		 * } else if (action.equalsIgnoreCase("listDriver")){ redirect = DriverRecord;
		 * request.setAttribute("drivers", dao.getAllDrivers()); }
		 */ else {
			redirect = Insert;
		}

		RequestDispatcher rd = request.getRequestDispatcher(redirect);
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}
