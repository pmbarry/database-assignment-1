package net.informatic.handler;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.informatic.bean.FleetBean;
import net.informatic.dao.FleetDao;

/**
 * Servlet implementation class FleetHandler
 */
public class FleetHandler extends HttpServlet { 
		private static final long serialVersionUID = 1L;
	
	    private static String Insert = "/fleet.jsp";
	    private static String Edit = "/editFleet.jsp";
	    private static String FleetRecord = "/listFleet.jsp";
	    private FleetDao dao;
	    
	    public FleetHandler() {
	        super();
	        dao = new FleetDao();
	    }

	    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    	
	    	Enumeration<String> params = request.getParameterNames(); 
	    	while(params.hasMoreElements()){
	    	 String paramName = params.nextElement();
	    	 System.out.println("Parameter Name - "+paramName+", Value - "+request.getParameter(paramName));
	    	}
	    	
	    	String redirect="";
	        String vehicleReg = request.getParameter("vehicle_reg");        
	        String action = request.getParameter("action");
	        
	        if(!((vehicleReg)== null) && action.equalsIgnoreCase("insert"))
	        {
	        	FleetBean fleet = new FleetBean();
	        	
	            fleet.setVehicleMake(request.getParameter("vehicle_make"));
	            fleet.setVehicleModel(request.getParameter("vehicle_model"));
	            fleet.setVehicleReg(request.getParameter("vehicle_reg"));
	            fleet.setVehicleOdo(Integer.parseInt(request.getParameter("vehicle_odo")));
	          
	        	dao.addFleet(fleet);
	        	redirect = FleetRecord;
	            request.setAttribute("fleet", dao.getAllFleet());    
	           	System.out.println("Record Added Successfully");
	        }
	        else if (action.equalsIgnoreCase("delete")){
	            String Id = request.getParameter("vehicleId");
	            System.out.println(Id);
	            int id = Integer.parseInt(Id);
	            dao.removeFleet(id);
	            redirect = FleetRecord;
	            request.setAttribute("fleet", dao.getAllFleet());
	            System.out.println("Record Deleted Successfully");
	            
	        }else if (action.equalsIgnoreCase("editform")){        	
	        	redirect = Edit; 
	        	
	        } else if (action.equalsIgnoreCase("edit")){
	        	String Id = request.getParameter("vehicle_id");
	            int id = Integer.parseInt(Id);            
	            FleetBean fleet = new FleetBean();
	        	 fleet.setVehicleId(id);
	        	 fleet.setVehicleMake(request.getParameter("vehicle_make"));
		         fleet.setVehicleModel(request.getParameter("vehicle_model"));
		         fleet.setVehicleReg(request.getParameter("vehicle_reg"));
		         fleet.setVehicleOdo(Integer.parseInt(request.getParameter("vehicle_odo")));
	            dao.editFleet(fleet);
	            request.setAttribute("fleet", fleet);
	            redirect = FleetRecord;
	            System.out.println("Record updated Successfully");
	            
	         } else if (action.equalsIgnoreCase("listFleet")){
	            redirect = FleetRecord;
	            request.setAttribute("fleet", dao.getAllFleet());
	         } else {
	            redirect = Insert;
	        }

	        RequestDispatcher rd = request.getRequestDispatcher(redirect);
	        rd.forward(request, response);
	    }

	    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	        doGet(request, response);
	    }

}
