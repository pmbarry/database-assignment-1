package net.informatic.handler;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.informatic.bean.DepotBean;
import net.informatic.dao.DepotDao;


/**
 * Servlet implementation class FleetHandler
 */
public class DepotHandler extends HttpServlet { 
		private static final long serialVersionUID = 1L;
	
	    private static String Insert = "/depot.jsp";
	    private static String Edit = "/editDepot.jsp";
	    private static String DepotRecord = "/listDepot.jsp";
	    private DepotDao dao;
	    
	    public DepotHandler() {
	        super();
	        dao = new DepotDao();
	    }

	    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    	
	    	Enumeration<String> params = request.getParameterNames(); 
	    	while(params.hasMoreElements()){
	    	 String paramName = params.nextElement();
	    	 System.out.println("Parameter Name - "+paramName+", Value - "+request.getParameter(paramName));
	    	}
	    	
	    	String redirect="";
	        String depotName = request.getParameter("depot_name");        
	        String action = request.getParameter("action");
	        
	        if(!((depotName) == null) && action.equalsIgnoreCase("insert"))
	        {
	        	
	        	DepotBean depotBean = new DepotBean();
	        	
	        	depotBean.setDepotName(request.getParameter("depot_name"));
	        	depotBean.setDepotDistance(Integer.parseInt(request.getParameter("depot_distance")));
	        	
	        	
	          
	        	dao.addDepot(depotBean);
	        	redirect = DepotRecord;
	            request.setAttribute("depots", dao.getAllDepots());    
	           	System.out.println("Record Added Successfully");
	        }
	        else if (action.equalsIgnoreCase("delete")){
	            String Id = request.getParameter("depot_id");
	            System.out.println(Id);
	            int id = Integer.parseInt(Id);
	            dao.removeDepot(id);
	            redirect = DepotRecord;
	            request.setAttribute("depots", dao.getAllDepots());
	            System.out.println("Record Deleted Successfully");
	            
	        }else if (action.equalsIgnoreCase("editform")){        	
	        	redirect = Edit; 
	        	
	        } else if (action.equalsIgnoreCase("edit")){
	        	String Id = request.getParameter("depot_id");
	            int id = Integer.parseInt(Id);            
	            DepotBean depotBean = new DepotBean();
	            depotBean.setDepotId(id);
	        	depotBean.setDepotName(request.getParameter("depot_name"));
	        	depotBean.setDepotDistance(Integer.parseInt(request.getParameter("depot_distance")));
	            dao.editDepot(depotBean);
	            request.setAttribute("depots", depotBean);
	            redirect = DepotRecord;
	            System.out.println("Record updated Successfully");
	            
	         } else if (action.equalsIgnoreCase("listDepot")){
	            redirect = DepotRecord;
	            request.setAttribute("depots", dao.getAllDepots());
	         } else {
	            redirect = Insert;
	        }

	        RequestDispatcher rd = request.getRequestDispatcher(redirect);
	        rd.forward(request, response);
	    }

	    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	        doGet(request, response);
	    }

}
