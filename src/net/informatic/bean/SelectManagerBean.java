package net.informatic.bean;

public class SelectManagerBean {

	public SelectManagerBean() {

	}

	private int managerId;
	private String managerName;

	public SelectManagerBean(int id, String name) {
		super();
		this.managerId = id;
		this.managerName = name;
	}

	public int getManagerId() {
		return managerId;
	}

	public void setManagerId(int managerId) {
		this.managerId = managerId;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

}
