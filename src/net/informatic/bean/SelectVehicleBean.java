package net.informatic.bean;

public class SelectVehicleBean {

	public SelectVehicleBean() {

	}

	private int vehicleId;
	private String vehicleReg;

	public SelectVehicleBean(int id, String reg) {
		super();
		this.vehicleId = id;
		this.vehicleReg = reg;
	}

	public int getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(int vehicleId) {
		this.vehicleId = vehicleId;
	}

	public String getVehicleReg() {
		return vehicleReg;
	}

	public void setVehicleReg(String vehicleReg) {
		this.vehicleReg = vehicleReg;
	}

}
