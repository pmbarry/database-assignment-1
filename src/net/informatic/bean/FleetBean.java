package net.informatic.bean;

public class FleetBean {
	
	private int vehicleId;
    private String vehicleMake;
    private String vehicleModel;
    private String vehicleReg;
    private int vehicleOdo;
    
    
	public int getVehicleId() {
		return vehicleId;
	}
	public void setVehicleId(int vehicleId) {
		this.vehicleId = vehicleId;
	}
	public String getVehicleMake() {
		return vehicleMake;
	}
	public void setVehicleMake(String vehicleMake) {
		this.vehicleMake = vehicleMake;
	}
	public String getVehicleModel() {
		return vehicleModel;
	}
	public void setVehicleModel(String vehicleModel) {
		this.vehicleModel = vehicleModel;
	}
	public String getVehicleReg() {
		return vehicleReg;
	}
	public void setVehicleReg(String vehicleReg) {
		this.vehicleReg = vehicleReg;
	}
	public int getVehicleOdo() {
		return vehicleOdo;
	}
	public void setVehicleOdo(int vehicleOdo) {
		this.vehicleOdo = vehicleOdo;
	}
    
    
   

}
