package net.informatic.bean;

public class TravelLogBean {
	
		
		private int travelLogId;
	    private int travelLogDepotId;
	    private String travelLogDate;
	    private String travelLogPurpose;
	    private int travelLogMiles;
	    private int travelLogFormId;
	    private int travelLogVehicleId;
	    
	    
	    
		public int getTravelLogId() 
		{
			return travelLogId;
		}
		public void setTravelLogId(int travelLogId) {
			this.travelLogId = travelLogId;
		}
		public int getTravelLogDepotId() {
			return travelLogDepotId;
		}
		public void setTravelLogDepotId(int travelLogDepotId) {
			this.travelLogDepotId = travelLogDepotId;
		}
		public String getTravelLogDate() {
			return travelLogDate;
		}
		public void setTravelLogDate(String travelLogDate) {
			this.travelLogDate = travelLogDate;
		}
		public String getTravelLogPurpose() {
			return travelLogPurpose;
		}
		public void setTravelLogPurpose(String travelLogPurpose) {
			this.travelLogPurpose = travelLogPurpose;
		}
		public int getTravelLogMiles() {
			return travelLogMiles;
		}
		public void setTravelLogMiles(int travelLogMiles) {
			this.travelLogMiles = travelLogMiles;
		}
		public int getTravelLogFormId() {
			return travelLogFormId;
		}
		public void setTravelLogFormId(int travelLogFormId) {
			this.travelLogFormId = travelLogFormId;
		}
		public int getTravelLogVehicleId() {
			return travelLogVehicleId;
		}
		public void setTravelLogVehicleId(int travelLogVehicleId) {
			this.travelLogVehicleId = travelLogVehicleId;
		}
	   
	    
	    
		

	}



