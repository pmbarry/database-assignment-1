package net.informatic.bean;

public class TravelFormBean {
	
		
		private int travelFormId;
	    private int travelFormDriver;
	    private String travelFormDate;
	    private int travelFormWeek;
	    private int travelFormMiles;
	    
	    
	    
		public int getTravelFormId() 
		{
			return travelFormId;
		}
		public void setTravelFormId(int travelFormId) {
			this.travelFormId = travelFormId;
		}
		public int getTravelFormDriver() {
			return travelFormDriver;
		}
		public void setTravelFormDriver(int travelFormDriver) {
			this.travelFormDriver = travelFormDriver;
		}
		public String getTravelFormDate() {
			return travelFormDate;
		}
		public void setTravelFormDate(String travelFormDate) {
			this.travelFormDate = travelFormDate;
		}
		public int getTravelFormWeek() {
			return travelFormWeek;
		}
		public void setTravelFormWeek(int travelFormWeek) {
			this.travelFormWeek = travelFormWeek;
		}
		public int getTravelFormMiles() {
			return travelFormMiles;
		}
		public void setTravelFormMiles(int travelFormMiles) {
			this.travelFormMiles = travelFormMiles;
		}
	   
	    
	    
		
	}



