package net.informatic.bean;

public class DepotBean {
	
	private int depotId;
    private String depotName;
    private int depotDistance;
    
	public int getDepotId() {
		return depotId;
	}
	public void setDepotId(int depotId) {
		this.depotId = depotId;
	}
	public String getDepotName() {
		return depotName;
	}
	public void setDepotName(String depotName) {
		this.depotName = depotName;
	}
	public int getDepotDistance() {
		return depotDistance;
	}
	public void setDepotDistance(int depotDistance) {
		this.depotDistance = depotDistance;
	}
    

}
