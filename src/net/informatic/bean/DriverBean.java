package net.informatic.bean;

public class DriverBean {
	
		
		private int driverId;
	    private String driverFirst;
	    private String driverSurname;
	    private int driverPoints;
	    private int driverAllowance;
	    private int driverManagerId;
	    private String driverUserName;
	    private String driverPassword;
	    
	    
		public int getDriverId() {
			return driverId;
		}
		public void setDriverId(int driverId) {
			this.driverId = driverId;
		}
		public String getDriverFirst() {
			return driverFirst;
		}
		public void setDriverFirst(String driverFirst) {
			this.driverFirst = driverFirst;
		}
		public String getDriverSurname() {
			return driverSurname;
		}
		public void setDriverSurname(String driverSurname) {
			this.driverSurname = driverSurname;
		}
		public int getDriverPoints() {
			return driverPoints;
		}
		public void setDriverPoints(int driverPoints) {
			this.driverPoints = driverPoints;
		}
		public int getDriverAllowance() {
			return driverAllowance;
		}
		public void setDriverAllowance(int driverAllowance) {
			this.driverAllowance = driverAllowance;
		}
		public int getDriverManagerId() {
			return driverManagerId;
		}
		public void setDriverManagerId(int driverManagerId) {
			this.driverManagerId = driverManagerId;
		}
		public String getDriverUserName() {
			return driverUserName;
		}
		public void setDriverUserName(String driverUserName) {
			this.driverUserName = driverUserName;
		}
		public String getDriverPassword() {
			return driverPassword;
		}
		public void setDriverPassword(String driverPassword) {
			this.driverPassword = driverPassword;
		}

	}



