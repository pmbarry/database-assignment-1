package net.informatic.bean;

public class ServiceBean {
	
	private int serviceId;
    private String serviceDate;
    private int serviceVehicleId;
    private int serviceOdometer ;
    
	public int getServiceId() {
		return serviceId;
	}
	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}
	public String getServiceDate() {
		return serviceDate;
	}
	public void setServiceDate(String serviceDate) {
		this.serviceDate = serviceDate;
	}
	public int getServiceVehicleId() {
		return serviceVehicleId;
	}
	public void setServiceVehicleId(int serviceVehicleId) {
		this.serviceVehicleId = serviceVehicleId;
	}
	public int getServiceOdometer() {
		return serviceOdometer;
	}
	public void setServiceOdometer(int serviceOdometer) {
		this.serviceOdometer = serviceOdometer;
	}
    

    

}
