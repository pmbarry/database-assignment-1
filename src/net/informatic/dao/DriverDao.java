package net.informatic.dao;

import java.util.*;
import java.sql.*;

import net.informatic.bean.DriverBean;
import net.informatic.dbconnection.ConnectionProvider;

public class DriverDao {
	private Connection conn;
	int driverId;

	public DriverDao() {
		conn = ConnectionProvider.getConnection();
	}

	public void addDriver(DriverBean driverBean) {
		System.out.println("in add Driver method of DAO");
		try {
			String sql = "INSERT INTO drivers(driver_first,driver_points,driver_allowance,driver_manager_id,driver_user,driver_pass)"
					+ " VALUES (?, ?, ?, ?, ?, ? ,?)";

			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setString(1, driverBean.getDriverFirst());
			ps.setString(2, driverBean.getDriverSurname());
			ps.setInt(3, driverBean.getDriverPoints());
			ps.setInt(4, driverBean.getDriverAllowance());
			ps.setInt(5, driverBean.getDriverManagerId());
			ps.setString(6, driverBean.getDriverUserName());
			ps.setString(7, driverBean.getDriverPassword());

			ps.executeUpdate();
			
            String Query="SELECT last_insert_id() from FleetManagement.drivers";
         	PreparedStatement st = conn.prepareStatement(Query);
           	ResultSet rs =st.executeQuery(Query);
           	
       	
            if (rs.next()) {
            	driverId = (rs.getInt(1));
                System.out.println("Driver ID is " + driverId);
                driverBean.setDriverId(driverId);
                rs.close();
                System.out.println("Record Added");}

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void removeDriver(int driverId) {
		try {
			String sql = "DELETE FROM drivers WHERE driver_id=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, driverId);
			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void editDriver(DriverBean driverBean) {
		try {
			String sql = "UPDATE drivers SET driver_first=?,driver_points=?,driver_allowance=?,driver_manager_id=?,driver_user=?,driver_pass=?"
					+ " WHERE driver_id=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, driverBean.getDriverFirst());
			ps.setString(2, driverBean.getDriverSurname());
			ps.setInt(3, driverBean.getDriverPoints());
			ps.setInt(4, driverBean.getDriverAllowance());
			ps.setInt(5, driverBean.getDriverManagerId());
			ps.setString(6, driverBean.getDriverUserName());
			ps.setString(7, driverBean.getDriverPassword());

			ps.setInt(8, driverBean.getDriverId());
			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List getAllDrivers() {
		List drivers = new ArrayList();
		try {
			String sql = "SELECT * FROM drivers";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DriverBean driverBean = new DriverBean();
				driverBean.setDriverId(rs.getInt("driver_id"));
				driverBean.setDriverFirst(rs.getString("driver_first"));
				driverBean.setDriverSurname(rs.getString("driver_surname"));
				driverBean.setDriverAllowance(rs.getInt("driver_allowance"));
				driverBean.setDriverManagerId(rs.getInt("driver_manager_id"));
				driverBean.setDriverPoints(rs.getInt("driver_points"));
				driverBean.setDriverUserName(rs.getString("driver_user"));
				driverBean.setDriverPassword(rs.getString("driver_pass"));

				drivers.add(driverBean);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return drivers;
	}

	public DriverBean getDriverById(int driverId) {
		DriverBean driverBean = new DriverBean();
		try {
			String sql = "SELECT * FROM drivers WHERE driver_id=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, driverId);
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				driverBean.setDriverId(rs.getInt("driver_id"));
				driverBean.setDriverFirst(rs.getString("driver_first"));
				driverBean.setDriverSurname(rs.getString("driver_surname"));
				driverBean.setDriverAllowance(rs.getInt("driver_allowance"));
				driverBean.setDriverManagerId(rs.getInt("driver_manager_id"));
				driverBean.setDriverPoints(rs.getInt("driver_points"));
				driverBean.setDriverUserName(rs.getString("driver_user"));
				driverBean.setDriverPassword(rs.getString("driver_pass"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return driverBean;
	}

}
