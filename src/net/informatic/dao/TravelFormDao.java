package net.informatic.dao;

import java.util.*;
import java.sql.*;


import net.informatic.bean.TravelFormBean;
import net.informatic.bean.TravelLogBean;
import net.informatic.dbconnection.ConnectionProvider;

public class TravelFormDao {
	private Connection conn;
	int formId;


    public TravelFormDao() {
    	conn = ConnectionProvider.getConnection();
    }

    public void addForm(TravelFormBean travelFormBean) {
    	System.out.println("in add Form method of DAO");
       
         	
           	
           	try {
           		
           		
        	
        	String sql = "INSERT INTO travelforms(travelform_driver_id,travelform_date,travelform_week,travelform_miles)" +
    		" VALUES ( ?, ?, ?, ?)";
        
            PreparedStatement ps = conn.prepareStatement(sql);
            
            
            ps.setInt(1, travelFormBean.getTravelFormDriver());
            ps.setString(2, travelFormBean.getTravelFormDate()); 
            ps.setInt(3, travelFormBean.getTravelFormWeek()); 
            ps.setInt(4, travelFormBean.getTravelFormMiles()); 
        
           
            ps.executeUpdate();
            System.out.println("Record Added");
            
            String Query="SELECT last_insert_id() from FleetManagement.travelforms";
         	PreparedStatement st = conn.prepareStatement(Query);
           	ResultSet rs =st.executeQuery(Query);
       	
            if (rs.next()) {
            	formId = (rs.getInt(1));
                System.out.println("travelForm ID IS " + formId);
                travelFormBean.setTravelFormId(formId);
                rs.close();}
            

        } catch (SQLException e) {
            e.printStackTrace();
        }
           	
        }
           	
           	public void addTravelLog(TravelLogBean travelLogBean) {
            	System.out.println("in add travellog method of DAO");
                try {
                	String sql = "INSERT INTO travelogs(travelog_depot_id,travelog_date,travelog_purpose,travelog_kilometers,travelog_travelform_id,travelog_vehicle_id)" +
            		" VALUES ( ?, ?, ?, ?, ?, ?)";
                
                    PreparedStatement ps = conn.prepareStatement(sql);
                    
                    /*ps.setInt(1, travelLogBean.getTravelLogId());*/
                    ps.setInt(1, travelLogBean.getTravelLogDepotId());
                    ps.setString(2, travelLogBean.getTravelLogDate()); 
                    ps.setString(3, travelLogBean.getTravelLogPurpose()); 
                    ps.setInt(4, travelLogBean.getTravelLogMiles()); 
                    ps.setInt(5, formId); 
                    ps.setInt(6, travelLogBean.getTravelLogVehicleId()); 
              
         
                    ps.executeUpdate();
                    System.out.println("Record Added");

                } catch (SQLException e) {
                    e.printStackTrace();
                }
    }

  /*  public void removeDriver(int driverId) {
        try {
        	String sql = "DELETE FROM drivers WHERE driver_id=?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, driverId);
            ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
      }

    public void editDriver(DriverBean driverBean) {    	
    	try {
    		String sql = "UPDATE drivers SET driver_first=?, driver_surname=?,driver_address_id=?,driver_points=?,driver_allowance=?,driver_manager_id=?,driver_user=?,driver_pass=?" +
            " WHERE driver_id=?";
            PreparedStatement ps = conn
                    .prepareStatement(sql);
            ps.setString(1, driverBean.getDriverFirst());
            ps.setString(2, driverBean.getDriverSurname());
            ps.setInt(3, driverBean.getDriverAddressId()); 
            ps.setInt(4, driverBean.getDriverPoints()); 
            ps.setInt(5, driverBean.getDriverAllowance()); 
            ps.setInt(6, driverBean.getDriverManagerId());
            ps.setString(7, driverBean.getDriverUserName()); 
            ps.setString(8, driverBean.getDriverPassword()); 
            
            ps.setInt(9, driverBean.getDriverId());
            ps.executeUpdate();            

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public List getAllDrivers() {
        List drivers = new ArrayList();
        try {
        	String sql = "SELECT * FROM drivers";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                DriverBean driverBean = new DriverBean();
                driverBean.setDriverId(rs.getInt("driver_id"));
                driverBean.setDriverFirst(rs.getString("driver_first"));
                driverBean.setDriverSurname(rs.getString("driver_surname"));
                driverBean.setDriverAddressId(rs.getInt("driver_address_id"));
                driverBean.setDriverAllowance(rs.getInt("driver_allowance"));
                driverBean.setDriverManagerId(rs.getInt("driver_manager_id"));
                driverBean.setDriverPoints(rs.getInt("driver_points"));
                driverBean.setDriverUserName(rs.getString("driver_user")); 
                driverBean.setDriverPassword(rs.getString("driver_pass"));
                drivers.add(driverBean);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return drivers;
    }

    public DriverBean getDriverById(int driverId) {
    	DriverBean driverBean = new DriverBean();
        try {
        	String sql = "SELECT * FROM drivers WHERE driver_id=?";
            PreparedStatement ps = conn.
                    prepareStatement(sql);
            ps.setInt(1, driverId);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
            	driverBean.setDriverId(rs.getInt("driver_id"));
            	driverBean.setDriverFirst(rs.getString("driver_first"));
            	driverBean.setDriverSurname(rs.getString("driver_surname"));
                driverBean.setDriverAddressId(rs.getInt("driver_address_id"));
                driverBean.setDriverAllowance(rs.getInt("driver_allowance"));
                driverBean.setDriverManagerId(rs.getInt("driver_manager_id"));
                driverBean.setDriverPoints(rs.getInt("driver_points"));
                driverBean.setDriverUserName(rs.getString("driver_user")); 
                driverBean.setDriverPassword(rs.getString("driver_pass"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return driverBean;
    }*/
	

}

