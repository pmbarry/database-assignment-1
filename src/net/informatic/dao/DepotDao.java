package net.informatic.dao;

import java.util.*;
import java.sql.*;

import net.informatic.bean.DepotBean;
import net.informatic.dbconnection.ConnectionProvider;

public class DepotDao {
	private Connection conn;
	int depotId;

    public DepotDao() {
    	conn = ConnectionProvider.getConnection();
    }

    public void addDepot(DepotBean depotBean) {
        try {
        	String sql = "INSERT INTO depots(depot_name,depot_distance)" +
    		" VALUES (?, ?)";
        
            PreparedStatement ps = conn.prepareStatement(sql);
            
            ps.setString(1, depotBean.getDepotName());
            ps.setInt(2, depotBean.getDepotDistance());
           
           
         
            
            ps.executeUpdate();
            String Query="SELECT last_insert_id() from FleetManagement.depots";
         	PreparedStatement st = conn.prepareStatement(Query);
           	ResultSet rs =st.executeQuery(Query);
           	
       	
            if (rs.next()) {
            	depotId = (rs.getInt(1));
                System.out.println("Depot ID IS " + depotId);
                depotBean.setDepotId(depotId);
                rs.close();
                System.out.println("Record Added");}

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeDepot(int depotId) {
        try {
        	String sql = "DELETE FROM depots WHERE depot_id=?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, depotId);
            ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
      }

    public void editDepot(DepotBean depotBean) {    	
    	try {
    		String sql = "UPDATE depots SET depot_name=?, depot_distance=?" +
            " WHERE depot_id=?";
    		
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, depotBean.getDepotName());
            ps.setInt(2, depotBean.getDepotDistance()); 
            ps.setInt(3, depotBean.getDepotId()); 
            ps.executeUpdate();            

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public List getAllDepots() {
        List depots = new ArrayList();
        try {
        	String sql = "SELECT * FROM depots";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                DepotBean depotBean = new DepotBean();
                depotBean.setDepotId(rs.getInt("depot_id"));
                depotBean.setDepotName(rs.getString("depot_name"));
                depotBean.setDepotDistance(rs.getInt("depot_distance"));
                 
                depots.add(depotBean);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return depots;
    }

    public DepotBean getDepotById(int depotId) {
    	DepotBean depotBean = new DepotBean();
        try {
        	String sql = "SELECT * FROM depots WHERE depot_id=?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, depotId);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
            	 depotBean.setDepotId(rs.getInt("depot_id"));
                 depotBean.setDepotName(rs.getString("depot_name"));
                 depotBean.setDepotDistance(rs.getInt("depot_distance")); 
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return depotBean;
    }

}
