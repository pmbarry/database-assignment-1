package net.informatic.dao;

import java.util.*;
import java.sql.*;

import net.informatic.bean.SelectVehicleBean;
import net.informatic.dbconnection.ConnectionProvider;

public class SelectVehicleDao {
	private Connection conn;

    public SelectVehicleDao() {
    	conn = ConnectionProvider.getConnection();
    }
    
    public List<SelectVehicleBean> list() throws SQLException {
    	System.out.println("Inside SelectVehicleDao");
        List<SelectVehicleBean> listVehicle = new ArrayList<>();
         
        try {
            String sql = "SELECT * FROM fleet ORDER BY vehicle_reg";
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery(sql);
             
            while (result.next()) {
                int id = result.getInt("vehicle_id");
                String reg = result.getString("vehicle_reg");
                SelectVehicleBean vehicle = new SelectVehicleBean(id, reg);
                     
                listVehicle.add(vehicle);
                
                
            }          
             
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw ex;
        }      
         
        return listVehicle;
    }

}
