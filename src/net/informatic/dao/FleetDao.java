package net.informatic.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import net.informatic.bean.FleetBean;
import net.informatic.dbconnection.ConnectionProvider;

public class FleetDao {
	private Connection conn;
	int fleetId;

    public FleetDao() {
    	conn = ConnectionProvider.getConnection();
    }

    public void addFleet(FleetBean fleetBean) {
        try {
        	String sql = "INSERT INTO fleet(vehicle_make,vehicle_model,vehicle_reg,vehicle_odo)" +
    		" VALUES (?, ?, ?, ?)";
        
            PreparedStatement ps = conn.prepareStatement(sql);
            
            ps.setString(1, fleetBean.getVehicleMake());
            ps.setString(2, fleetBean.getVehicleModel()); 
            ps.setString(3, fleetBean.getVehicleReg()); 
            ps.setInt(4, fleetBean.getVehicleOdo()); 
         
            
            ps.executeUpdate();
            
            String Query="SELECT last_insert_id() from FleetManagement.fleet";
         	PreparedStatement st = conn.prepareStatement(Query);
           	ResultSet rs =st.executeQuery(Query);
           	
       	
            if (rs.next()) {
            	fleetId = (rs.getInt(1));
                System.out.println("Driver ID is " + fleetId);
                fleetBean.setVehicleId(fleetId);
                rs.close();
                System.out.println("Record Added");}

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeFleet(int vehicleId) {
        try {
        	String sql = "DELETE FROM fleet WHERE vehicle_id=?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, vehicleId);
            ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
      }

    public void editFleet(FleetBean fleetBean) {    	
    	try {
    		String sql = "UPDATE fleet SET vehicle_make=?, vehicle_model=?, vehicle_reg=?, vehicle_odo=?" +
            " WHERE vehicle_id=?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, fleetBean.getVehicleMake());
            ps.setString(2, fleetBean.getVehicleModel()); 
            ps.setString(3, fleetBean.getVehicleReg()); 
            ps.setInt(4, fleetBean.getVehicleOdo());
            ps.setInt(5, fleetBean.getVehicleId()); 
            ps.executeUpdate();            

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public List getAllFleet() {
        List fleet = new ArrayList();
        try {
        	String sql = "SELECT * FROM fleet";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                FleetBean fleetBean = new FleetBean();
                fleetBean.setVehicleId(rs.getInt("vehicle_id"));
                fleetBean.setVehicleMake(rs.getString("vehicle_Make"));
                fleetBean.setVehicleModel(rs.getString("vehicle_model"));
                fleetBean.setVehicleReg(rs.getString("vehicle_reg")); 
                fleetBean.setVehicleOdo(Integer.parseInt(rs.getString("vehicle_odo"))); 
                fleet.add(fleetBean);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return fleet;
    }

    public FleetBean getFleetById(int vehicleId) {
    	FleetBean fleetBean = new FleetBean();
        try {
        	String sql = "SELECT * FROM fleet WHERE vehicle_id=?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, vehicleId);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
            	fleetBean.setVehicleId(rs.getInt("vehicle_id"));
            	fleetBean.setVehicleMake(rs.getString("vehicle_make"));
            	fleetBean.setVehicleModel(rs.getString("vehicle_model"));
            	fleetBean.setVehicleReg(rs.getString("vehicle_reg")); 
                fleetBean.setVehicleOdo(Integer.parseInt(rs.getString("vehicle_odo"))); 
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return fleetBean;
    }

}
