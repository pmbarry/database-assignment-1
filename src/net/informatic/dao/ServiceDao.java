package net.informatic.dao;

import java.util.*;
import java.sql.*;

import net.informatic.bean.ServiceBean;
import net.informatic.dbconnection.ConnectionProvider;

public class ServiceDao {
	private Connection conn;
	int serviceId;

    public ServiceDao() {
    	conn = ConnectionProvider.getConnection();
    }

    public void addDepot(ServiceBean serviceBean) {
        try {
        	String sql = "INSERT INTO services(service_date,service_vehicle_id,service_odo)" +
    		" VALUES (?, ?, ?)";
        
            PreparedStatement ps = conn.prepareStatement(sql);
            
            
            ps.setString(1, serviceBean.getServiceDate());
            ps.setInt(2, serviceBean.getServiceVehicleId()); 
            ps.setInt(3, serviceBean.getServiceOdometer());

            ps.executeUpdate();
            
            String Query="SELECT last_insert_id() from FleetManagement.services";
         	PreparedStatement st = conn.prepareStatement(Query);
           	ResultSet rs =st.executeQuery(Query);
           	
       	
            if (rs.next()) {
            	serviceId = (rs.getInt(1));
                System.out.println("Service ID is " + serviceId);
                serviceBean.setServiceId(serviceId);
                rs.close();
                System.out.println("Record Added");}

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeService(int serviceId) {
        try {
        	String sql = "DELETE FROM services WHERE service_id=?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, serviceId);
            ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
      }

    public void editService(ServiceBean serviceBean) {    	
    	try {
    		String sql = "UPDATE services SET service_date=?, service_vehicle_id=?, service_odo=?" +
            " WHERE service_id=?";
    		
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, serviceBean.getServiceDate());
            ps.setInt(2, serviceBean.getServiceVehicleId()); 
            ps.setInt(3, serviceBean.getServiceOdometer()); 
            ps.setInt(4, serviceBean.getServiceId()); 
            ps.executeUpdate();            

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public List getAllServices() {
        List services = new ArrayList();
        try {
        	String sql = "SELECT * FROM services";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ServiceBean serviceBean = new ServiceBean();
                serviceBean.setServiceId(rs.getInt("service_id"));
                serviceBean.setServiceDate(rs.getString("service_date"));
                serviceBean.setServiceVehicleId(rs.getInt("service_vehicle_id"));
                serviceBean.setServiceOdometer(rs.getInt("service_odo"));
                 
                services.add(serviceBean);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return services;
    }

    public ServiceBean getServiceById(int serviceId) {
    	ServiceBean serviceBean = new ServiceBean();
        try {
        	String sql = "SELECT * FROM services WHERE service_id=?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, serviceId);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
            	   serviceBean.setServiceId(rs.getInt("service_id"));
                   serviceBean.setServiceDate(rs.getString("service_date"));
                   serviceBean.setServiceVehicleId(rs.getInt("service_vehicle_id"));
                   serviceBean.setServiceOdometer(rs.getInt("service_odo"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return serviceBean;
    }

}
