package net.informatic.dao;

import java.util.*;
import java.sql.*;

import net.informatic.bean.UserBean;
import net.informatic.dbconnection.ConnectionProvider;

public class UserDao {
	static Connection conn = ConnectionProvider.getConnection();

	public UserDao() {

	}

	public static UserBean login(UserBean bean) {

		// preparing some objects for connection
		Statement stmt = null;
		ResultSet rs1 = null;

		String username = bean.getUsername();
		String password = bean.getPassword();
		boolean manager = bean.isManager();

		String searchQuery1 = "select * from drivers where driver_user='" + username + "' AND driver_pass='" + password
				+ "'";

		String searchQuery2 = "select * from managers where manager_user='" + username + "' AND manager_pass='"
				+ password + "'";

		// "System.out.println" prints in the console; Normally used to trace the
		// process
		System.out.println("Your user name is " + username);
		System.out.println("Your password is " + password);
		System.out.println("Query: " + searchQuery1);
		System.out.println("Query: " + searchQuery2);
		try {
			// connect to DB

			stmt = conn.createStatement();
			rs1 = stmt.executeQuery(searchQuery1);
			boolean more = rs1.next();
			System.out.println(more);
			if (!more) {
				rs1.close();
				rs1 = stmt.executeQuery(searchQuery2);
				manager = rs1.next();
				more = manager;
				System.out.println("Manager Status"+manager);
				System.out.println("more Status in if"+more);
			}
			
			

			// if user does not exist set the isValid variable to false
			
			if (!more) {
				System.out.println("Sorry, you are not a registered user! Please sign up first");
				bean.setValid(false);
			}

			// if user exists set the isValid variable to true
			else if (more) {
				
				String firstName;
				String lastName;
				
				if(!manager) {
				 firstName= rs1.getString("driver_first");
				 lastName= rs1.getString("driver_surname");}
				
				else {
					firstName = rs1.getString("manager_first");
					lastName = rs1.getString("manager_surname");
				}
				

				System.out.println("Welcome " + firstName);
				bean.setFirstName(firstName);
				bean.setLastName(lastName);
				bean.setValid(true);
				bean.setManager(manager);
			}
		}

		catch (Exception ex) {
			System.out.println("Log In failed: An Exception has occurred! " + ex);
		}

		// some exception handling
		finally {
			if (rs1 != null) {
				try {
					rs1.close();
				} catch (Exception e) {
				}
				rs1 = null;
			}

			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception e) {
				}
				stmt = null;
			}

			/*if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
				}

				conn = null;
			}*/
		}

		return bean;

	}
}
