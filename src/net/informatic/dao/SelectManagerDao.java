package net.informatic.dao;

import java.util.*;
import java.sql.*;

import net.informatic.bean.SelectManagerBean;
import net.informatic.dbconnection.ConnectionProvider;

public class SelectManagerDao {
	private Connection conn;

    public SelectManagerDao() {
    	conn = ConnectionProvider.getConnection();
    }
    
    public List<SelectManagerBean> list() throws SQLException {
    	System.out.println("Inside SelectManagerDao");
        List<SelectManagerBean> listManager = new ArrayList<>();
         
        try {
            String sql = "SELECT * FROM managers ORDER BY manager_id";
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery(sql);
             
            while (result.next()) {
                int id = result.getInt("manager_id");
                String name= (result.getString("manager_first")+" "+result.getString("manager_surname"));
                SelectManagerBean manager = new SelectManagerBean(id, name);
                     
                listManager.add(manager);
                
                
            }          
             
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw ex;
        }      
         
        return listManager;
    }

}
