<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ page import="net.informatic.bean.ServiceBean"%>
<%@ page import="net.informatic.dao.ServiceDao"%>
<%@ page import="java.util.*"%>
<html>
<head>
<meta charset="UTF-8">
<title>All Depots</title>
</head>
<body>
	<%
		/* DriverBean driver = new DriverBean(); */
		ServiceDao dao = new ServiceDao();
		List<ServiceBean> serviceList = dao.getAllServices();
		/* Iterator<DriverBean> itr = driverList.iterator(); */
	%>

	<table border="1">
		<tr>
			<th>Service ID</th>
			<th>Service Date</th>
			<th>Service Vehicle ID</th>
			<th>Service Odometer</th>
		</tr>
		<tr>
			<%
				/* while (itr.hasNext()) {
					System.out.println(driver.getDriverId()); */
				for (ServiceBean services : serviceList) {
			%>
			<td><%=services.getServiceId()%></td>
			<td><%=services.getServiceDate()%></td>
			<td><%=services.getServiceVehicleId()%></td>
			<td><%=services.getServiceOdometer()%></td>		
			<td><a
				href="ServiceHandler?action=editform&service_id=<%=services.getServiceId()%>&service_date=<%=services.getServiceDate()%>">Update</a></td>
			<td><a
				href="ServiceHandler?action=delete&service_id=<%=services.getServiceId()%>&service_date=<%=services.getServiceDate()%>">Delete</a></td>

		</tr>
		<%
			}
		%>




	</table>
	<p>
		<a href="ServiceHandler?action=insert">Add Service</a>
		<a href="welcome.jsp">Home</a>
	</p>

</body>
</html>