<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ page import="net.informatic.bean.DepotBean"%>
<%@ page import="net.informatic.dao.DepotDao"%>
<%@ page import="java.util.*"%>
<html>
<head>
<meta charset="UTF-8">
<title>All Depots</title>
</head>
<body>
	<%
		/* DriverBean driver = new DriverBean(); */
		DepotDao dao = new DepotDao();
		List<DepotBean> depotList = dao.getAllDepots();
		/* Iterator<DriverBean> itr = driverList.iterator(); */
	%>

	<table border="1">
		<tr>
			<th>Depot ID</th>
			<th>Depot Name</th>
			<th>Depot Distance</th>
		</tr>
		<tr>
			<%
			
				for (DepotBean depots : depotList) {
			%>
			<td><%=depots.getDepotId()%></td>
			<td><%=depots.getDepotName()%></td>
			<td><%=depots.getDepotDistance()%></td>		
			<td><a
				href="DepotHandler?action=editform&depot_id=<%=depots.getDepotId()%>&depot_name=<%=depots.getDepotName()%>">Update</a></td>
			<td><a
				href="DepotHandler?action=delete&depot_id=<%=depots.getDepotId()%>&depot_name=<%=depots.getDepotName()%>">Delete</a></td>

		</tr>
		<%
			}
		%>




	</table>
	<p>
		<a href="DepotHandler?action=insert">Add Depot</a>
		<a href="welcome.jsp">Home</a>
	</p>

</body>
</html>