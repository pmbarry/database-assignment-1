<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ page import="net.informatic.bean.FleetBean"%>
<%@ page import="net.informatic.dao.FleetDao"%>
<%@ page import="java.util.*"%>
<html>
<head>
<meta charset="UTF-8">
<title>All Vehicles</title>
</head>
<body>
	<%
		FleetDao dao = new FleetDao();
		List<FleetBean> fleetList = dao.getAllFleet();
	%>

	<table border="1">
		<tr>
			<th>Vehicle ID</th>
			<th>Vehicle Make</th>
			<th>Vehicle Model</th>
			<th>Vehicle Registration</th>
			<th>Vehicle Odometer</th>
		</tr>
		<tr>
			<%
				for (FleetBean vehicles : fleetList) {
			%>
			<td><%=vehicles.getVehicleId()%></td>
			<td><%=vehicles.getVehicleMake()%></td>
			<td><%=vehicles.getVehicleModel()%></td>
			<td><%=vehicles.getVehicleReg()%></td>
			<td><%=vehicles.getVehicleOdo()%></td>
		
			
			<td><a
				href="FleetHandler?action=editform&vehicleId=<%=vehicles.getVehicleId()%>&vehicle_reg=<%=vehicles.getVehicleReg()%>">Update</a></td>
			<td><a
				href="FleetHandler?action=delete&vehicleId=<%=vehicles.getVehicleId()%>&vehicle_reg=<%=vehicles.getVehicleReg()%>">Delete</a></td>

		</tr>
		<%
			}
		%>




	</table>
	<p>
		<a href="FleetHandler?action=insert">Add Vehicle</a>
		<a href="welcome.jsp">Home</a>
	</p>

</body>
</html>