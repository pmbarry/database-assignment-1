<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Add New Fleet Vehicle</title>
</head>
<body>
<form method="POST" action='FleetHandler' name="frmAddFleet"><input
type="hidden" name="action" value="insert" />
<p><b>Add New Record</b></p>
<table>
<tr>
<td>Vehicle Make</td>
<td><input type="text" name="vehicle_make" /></td>
</tr>
<tr>
<td>Vehicle Model</td>
<td><input type="text" name="vehicle_model" /></td>
</tr>

<tr>
<td>Vehicle Registration</td>
<td><input type="text" name="vehicle_reg" /></td>
</tr>

<tr>
<td>Vehicle Mileage</td>
<td><input type="text" name="vehicle_odo" /></td>
</tr>

<tr>
<td></td>
<td><input type="submit" value="Submit" /></td>
</tr>
</table>
</form>
<p><a href="FleetHandler?action=listFleet">View-All-Records</a></p>
</body>
</html>