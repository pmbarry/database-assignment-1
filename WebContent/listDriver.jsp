<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ page import="net.informatic.bean.DriverBean"%>
<%@ page import="net.informatic.dao.DriverDao"%>
<%@ page import="java.util.*"%>
<html>
<head>
<meta charset="UTF-8">
<title>All Drivers</title>
</head>
<body>
	<%
		/* DriverBean driver = new DriverBean(); */
		DriverDao dao = new DriverDao();
		List<DriverBean> driverList = dao.getAllDrivers();
		/* Iterator<DriverBean> itr = driverList.iterator(); */
	%>
	<table border="1">
		<tr>
			<th>Driver ID</th>
			<th>Driver Name</th>
			<th>Driver Surname</th>
			<th>Driver Points</th>
			<th>Driver Allowance</th>
			<th>Driver Manager</th>
			<th>Driver Username</th>
			<th>Driver Password</th>
		</tr>
		<tr>
			<%
				/* while (itr.hasNext()) {
					System.out.println(driver.getDriverId()); */
				for (DriverBean drivers : driverList) {
			%>
			<td><%=drivers.getDriverId()%></td>
			<td><%=drivers.getDriverFirst()%></td>
			<td><%=drivers.getDriverSurname()%></td>
			<td><%=drivers.getDriverPoints()%></td>
			<td><%=drivers.getDriverAllowance()%></td>
			<td><%=drivers.getDriverManagerId()%></td>
			<td><%=drivers.getDriverUserName()%></td>
			<td><%=drivers.getDriverPassword()%></td>




			<td><a
				href="DriverHandler?action=editform&driverId=<%=drivers.getDriverId()%>&driver_firstname=<%=drivers.getDriverFirst()%>">Update</a></td>
			<td><a
				href="DriverHandler?action=delete&driverId=<%=drivers.getDriverId()%>&driver_firstname=<%=drivers.getDriverFirst()%>">Delete</a></td>

		</tr>
		<%
			}
		%>




	</table>
	<p>
		<a href="DriverHandler?action=insert">Add Driver</a>
		<a href="welcome.jsp">Home</a>
	</p>

</body>
</html>