<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ page import="net.informatic.bean.ServiceBean"%>
<%@ page import="net.informatic.dao.ServiceDao"%>
<html>
<head>
<meta charset="UTF-8">
<title>Edit Service Details</title>
</head>
<body>
<%
ServiceBean serviceBean = new ServiceBean();
%>
<%
ServiceDao dao = new ServiceDao();
%>
<form method="POST" action='ServiceHandler' name="frmEditService"><input
type="hidden" name="action" value="edit" /> <%
String id = request.getParameter("service_id");
if (!((id) == null)) {
int sid = Integer.parseInt(id);
serviceBean = dao.getServiceById(sid);

%>
<table>
<tr>
<td>Service ID</td>
<td><input type="text" name="service_id" readonly="readonly"
value="<%=serviceBean.getServiceId()%>"></td>
</tr>
<tr>
<td>Service Date</td>
<td><input type="text" name="service_date" value="<%=serviceBean.getServiceDate()%>" /></td>
</tr>
<tr>
<td>Service Vehicle ID</td>
<td><input type="text" name="service_vehicle_id" value="<%=serviceBean.getServiceVehicleId()%>"/></td>
</tr>
<tr>
<td>Service Odometer</td>
<td><input type="text" name="service_odometer" value="<%=serviceBean.getServiceOdometer()%>"/></td>
</tr>

<tr>
<td></td>
<td><input type="submit" value="Update" /></td>
</tr>
</table>
<%
} else
out.println("ID Not Found");
%>
</form>
</body>
</html>