<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ page import="net.informatic.bean.DriverBean"%>
<%@ page import="net.informatic.dao.DriverDao"%>
<html>
<head>
<meta charset="UTF-8">
<title>Edit Driver Details</title>
</head>
<body>
<%
DriverBean driver = new DriverBean();
%>
<%
DriverDao dao = new DriverDao();
%>
<form method="POST" action='DriverHandler' name="frmEditDriver"><input
type="hidden" name="action" value="edit" /> <%
String id = request.getParameter("driverId");
if (!((id) == null)) {
int did = Integer.parseInt(id);
driver = dao.getDriverById(did);
%>
<table>
<tr>
<td>Driver ID</td>
<td><input type="text" name="driver_id" readonly="readonly"
value="<%=driver.getDriverId()%>"></td>
</tr>
<tr>
<td>Driver First Name</td>
<td><input type="text" name="driver_firstname" value="<%=driver.getDriverFirst()%>" /></td>
</tr>
<tr>
<td>Driver Surname</td>
<td><input type="text" name="driver_surname" value="<%=driver.getDriverSurname()%>"/></td>
</tr>
<tr>
<td>Driver Points</td>
<td><input type="text" name="driver_points" value="<%=driver.getDriverPoints()%>"/></td>
</tr>
<tr>
<td>Driver Allowance</td>
<td><input type="text" name="driver_allowance" value="<%=driver.getDriverAllowance()%>"/></td>
</tr>
<tr>
<td>Driver Manager ID</td>
<td><input type="text" name="driver_manager_id" value="<%=driver.getDriverManagerId()%>"/></td>
</tr>
<tr>
<td>Driver User Name</td>
<td><input type="text" name="driver_user" value="<%=driver.getDriverUserName()%>"/></td>
</tr>
<tr>
<td>Driver Password</td>
<td><input type="text" name="driver_password" value="<%=driver.getDriverPassword()%>"/></td>
</tr>
<tr>
<td></td>
<td><input type="submit" value="Update" /></td>
</tr>
</table>
<%
} else
out.println("ID Not Found");
%>
</form>
</body>
</html>