<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ page import="net.informatic.bean.FleetBean"%>
<%@ page import="net.informatic.dao.FleetDao"%>
<html>
<head>
<meta charset="UTF-8">
<title>Edit Vehicle Details</title>
</head>
<body>
<%
FleetBean fleetBean = new FleetBean();
%>
<%
FleetDao dao = new FleetDao();
%>
<form method="POST" action='FleetHandler' name="frmEditFleet"><input
type="hidden" name="action" value="edit" /> <%
String id = request.getParameter("vehicleId");
if (!((id) == null)) {
int vid = Integer.parseInt(id);
fleetBean = dao.getFleetById(vid);

%>
<table>
<tr>
<td>Vehicle ID</td>
<td><input type="text" name="vehicle_id" readonly="readonly"
value="<%=fleetBean.getVehicleId()%>"></td>
</tr>
<tr>
<td>Vehicle Make</td>
<td><input type="text" name="vehicle_make" value="<%=fleetBean.getVehicleMake()%>" /></td>
</tr>
<tr>
<td>Vehicle Model</td>
<td><input type="text" name="vehicle_model" value="<%=fleetBean.getVehicleModel()%>"/></td>
</tr>
<tr>
<td>Vehicle Registration</td>
<td><input type="text" name="vehicle_reg" value="<%=fleetBean.getVehicleReg()%>"/></td>
</tr>
<tr>
<td>Vehicle Odometer</td>
<td><input type="text" name="vehicle_odo" value="<%=fleetBean.getVehicleOdo()%>"/></td>
</tr>
<tr>
<td></td>
<td><input type="submit" value="Update" /></td>
</tr>
</table>
<%
} else
out.println("ID Not Found");
%>
</form>
</body>
</html>