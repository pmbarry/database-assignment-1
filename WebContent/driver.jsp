<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="net.informatic.dao.FleetDao"%>
<%@ page import="net.informatic.dao.SelectManagerDao"%>
<%@ page import="net.informatic.bean.SelectManagerBean"%>
<%@ page import="java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Add New Driver</title>
</head>
<body>
<%
		SelectManagerDao managersDao;
		managersDao = new SelectManagerDao();

		List<SelectManagerBean> managers = new ArrayList();
		managers = managersDao.list();
		for (SelectManagerBean manager : managers) {
			System.out.println(manager.getManagerName());
		}
	%>
<form method="POST" action='DriverHandler' name="frmAddDriver"><input
type="hidden" name="action" value="insert" />
<p><b>Add New Record</b></p>
<table>
<tr>
<td>First Name</td>
<td><input type="text" name="driver_firstname" /></td>
</tr>
<tr>
<td>Last Name</td>
<td><input type="text" name="driver_surname" /></td>
</tr>

<tr>
<td>Points</td>
<td><input type="text" name="driver_points" /></td>
</tr>

<tr>
<td>Allowance</td>
<td><input type="text" name="driver_allowance" /></td>
</tr>

<tr>
				<td>Select Manager</td>
				<td><select id="managerSelect" name="manager_id">
						<option value="SELECT">Select Manager</option>
						<%
							for (SelectManagerBean vehicle : managers) {
						%>
						<option value="<%=vehicle.getManagerId()%>"><%=vehicle.getManagerName()%></option>
						<%
							}
						%>


				</select></td>
</tr>

<tr>
<td>User name</td>
<td><input type="text" name="driver_user" /></td>
</tr>

<tr>
<td>Password</td>
<td><input type="text" name="driver_password" /></td>
</tr>

<tr>
<td></td>
<td><input type="submit" value="Submit" /></td>
</tr>
</table>
</form>
<p><a href="DriverHandler?action=listDriver">View-All-Records</a></p>
</body>
</html>