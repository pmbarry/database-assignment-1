<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<head>
<meta charset="UTF-8">
<title>Weekly Travel Form</title>
</head>
<body>
<Script>
     $(document).ready(function(){
      var i=1;
     $("#add_row").click(function(){
      $('#addr'+i).html("<td>"+ (i+1) +"</td><td><input name='Date"+i+"' type='text' placeholder='Date' class='form-control input-md'  /> </td><td><input  name='Depot"+i+"' type='text' placeholder='Depot'  class='form-control input-md'></td><td><input  name='Vehicle"+i+"' type='text' placeholder='Vehicle'  class='form-control input-md'></td><td><input  name='Purpose"+i+"' type='text' placeholder='Purpose'  class='form-control input-md'></td><td><input  name='Kilometers"+i+"' type='text' placeholder='Kilometers'  class='form-control input-md'></td>");
      document.getElementById("logcount").value=i; 
      $('#tab_logic').append('<tr id="addr'+(i+1)+'">');
      i++; 
  });
     $("#delete_row").click(function(){
    	 if(i>1){
		 $("#addr"+(i-1)).html('');
		 i--;
		 document.getElementById("logcount").value=i;
		 }
	 });

});
     
 </Script>
 <form method="POST" action='TravelFormHandler' name="frmAddTrvlFrm"><input
type="hidden" name="action" value="insert" />
 <div class="container">
    <div class="row clearfix">
		<div class="col-md-12 column">
			<table class="table table-bordered table-hover" id="tab_head">
				<thead>
					<tr >
						<th class="text-center">
							Driver
						</th>
						<th class="text-center">
							Date
						</th>
						<th class="text-center">
							Week
						</th>
						<th class="text-center">
							Miles
						</th>
						
					</tr>
					
				</thead>
				<tbody>
				
				<tr><td><input type='hidden' id='logcount' name='log-count' value=''></td></tr>
					<tr id='addr0'>
						
						<td>
						<input type="text" name='Driver'  placeholder='Driver' class="form-control"/>
						</td>
						<td>
						<input type="text" name='Date' placeholder='Date' class="form-control"/>
						</td>
						<td>
						<input type="text" name='Week' placeholder='Week' class="form-control"/>
						</td>
						<td>
						<input type="text" name='Miles' placeholder='Miles' class="form-control"/>
						</td>
					</tr>
                    
				</tbody>
			</table>
		</div>
	</div>
	
</div>
 
<div class="container">
    <div class="row clearfix">
		<div class="col-md-12 column">
			<table class="table table-bordered table-hover" id="tab_logic">
				<thead>
					<tr >
						<th class="text-center">
							#
						</th>
						<th class="text-center">
							Date
						</th>
						<th class="text-center">
							Depot
						</th>
						<th class="text-center">
							Vehicle
						</th>
						<th class="text-center">
							Purpose
						</th>
						<th class="text-center">
							Kilometers
						</th>
					</tr>
				</thead>
				<tbody id='log-tab'>
					<tr id='addr0'>
						<td>
						1
						</td>
						<td>
						<input type="text" name='Date0'  placeholder='Date' class="form-control"/>
						</td>
						<td>
						<input type="text" name='Depot0' placeholder='Depot' class="form-control"/>
						</td>
						<td>
						<input type="text" name='Vehicle0' placeholder='Vehicle' class="form-control"/>
						</td>
						<td>
						<input type="text" name='Purpose0' placeholder='Purpose' class="form-control"/>
						</td>
						<td>
						<input type="text" name='Kilometers0' placeholder='Kilometers' class="form-control"/>
						</td>
					</tr>
                    <tr id='addr1'><td>
						2
						</td>
						<td>
						<input type="text" name='Date1'  placeholder='Date' class="form-control"/>
						</td>
						<td>
						<input type="text" name='Depot1' placeholder='Depot' class="form-control"/>
						</td>
						<td>
						<input type="text" name='Vehicle1' placeholder='Vehicle' class="form-control"/>
						</td>
						<td>
						<input type="text" name='Purpose1' placeholder='Purpose' class="form-control"/>
						</td>
						<td>
						<input type="text" name='Kilo1' placeholder='Kilometers' class="form-control"/>
						</td></tr>
                    
				</tbody>
			</table>
		</div>
	</div>
	<a id="add_row" class="btn btn-default pull-left">Add Row</a><a id='delete_row' class="pull-right btn btn-default">Delete Row</a>
</div>
<input type="submit" />
</form>

</body>
</html>