<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ page import="net.informatic.bean.DepotBean"%>
<%@ page import="net.informatic.dao.DepotDao"%>
<html>
<head>
<meta charset="UTF-8">
<title>Edit Depot Details</title>
</head>
<body>
<%
DepotBean depotBean = new DepotBean();
%>
<%
DepotDao dao = new DepotDao();
%>
<form method="POST" action='DepotHandler' name="frmEditDepot"><input
type="hidden" name="action" value="edit" /> <%
String id = request.getParameter("depot_id");
if (!((id) == null)) {
int did = Integer.parseInt(id);
depotBean = dao.getDepotById(did);

%>
<table>
<tr>
<td>Depot ID</td>
<td><input type="text" name="depot_id" readonly="readonly"
value="<%=depotBean.getDepotId()%>"></td>
</tr>
<tr>
<td>Depot Name</td>
<td><input type="text" name="depot_name" value="<%=depotBean.getDepotName()%>" /></td>
</tr>
<tr>
<td>Depot Distance</td>
<td><input type="text" name="depot_distance" value="<%=depotBean.getDepotDistance()%>"/></td>
</tr>

<tr>
<td></td>
<td><input type="submit" value="Update" /></td>
</tr>
</table>
<%
} else
out.println("ID Not Found");
%>
</form>
</body>
</html>