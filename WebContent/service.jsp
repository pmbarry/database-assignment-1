<%@page import="java.util.ArrayList"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<%@ page import="net.informatic.bean.FleetBean"%>
<%@ page import="net.informatic.dao.FleetDao"%>
<%@ page import="net.informatic.dao.SelectVehicleDao"%>
<%@ page import="net.informatic.bean.SelectVehicleBean"%>
<%@ page import="java.util.*"%>
<html>



<head>
<meta charset="UTF-8">
<title>Add New Service</title>
</head>
<body>
	<%
		SelectVehicleDao vehiclesDao;
		vehiclesDao = new SelectVehicleDao();

		List<SelectVehicleBean> vehicles = new ArrayList();
		vehicles = vehiclesDao.list();
		for (SelectVehicleBean vehicle : vehicles) {
			System.out.println(vehicle.getVehicleReg());
		}
	%>

	<form method="POST" action='ServiceHandler' name="frmAddService">
		<input type="hidden" name="action" value="insert" />
		<p>
			<b>Add New Service</b>
		</p>


		<table>

			<tr>
				<td>Service Date</td>
				<td><input type="text" name="service_date" /></td>
			</tr>
			<tr>
				<td>Service Vehicle</td>
				<td><select id="vehicleSelect" name="service_vehicle_id">
						<option value="SELECT">Select Vehicle</option>
						<%
							for (SelectVehicleBean vehicle : vehicles) {
						%>
						<option value="<%=vehicle.getVehicleId()%>"><%=vehicle.getVehicleReg()%></option>
						<%
							}
						%>


				</select></td>
			</tr>

			<tr>
				<td>Service Odometer</td>
				<td><input type="text" name="service_odo" /></td>
			</tr>

			<tr>
				<td></td>
				<td><input type="submit" value="Submit" /></td>
			</tr>
		</table>
	</form>
	<p>
		<a href="ServiceHandler?action=listService">View-All-Records</a>
	</p>
</body>
</html>