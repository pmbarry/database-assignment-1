<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Add New Depot</title>
</head>
<body>
<form method="POST" action='DepotHandler' name="frmAddDepot">

<p><b>Add New Depot</b></p>
<table>
<tr>
<td>Depot Name</td>
<td><input type="text" name="depot_name" /></td>
</tr>
<tr>
<td>Depot Distance</td>
<td><input type="text" name="depot_distance" /></td>
</tr>

<tr>
<td></td>
<td><input type="submit" value="Submit" /></td>
</tr>
</table>
</form>
<p><a href="DepotHandler?action=listDepot">View-All-Records</a></p>
</body>
</html>